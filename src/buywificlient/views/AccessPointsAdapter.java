package buywificlient.views;

import java.util.ArrayList;
import java.util.List;

import com.example.buywificlient.R;

import buywificlient.MainActivity;
import buywificlient.wifi.APList;
import buywificlient.wifi.WifiAP;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

public class AccessPointsAdapter extends ArrayAdapter<WifiAP> {
	 int resource;
	    String response;
	    Context context;

	    public AccessPointsAdapter(Context context, int resource, List<WifiAP> items) {
	        super(context, resource, items);
	        this.resource=resource;
	 
	    }
	     
	     
	    @Override
	    public View getView(int position, View convertView, ViewGroup parent)
	    {
	        LinearLayout alertView;
	        WifiAP al = getItem(position);
	         
	        if(convertView==null)
	        {
	            alertView = new LinearLayout(getContext());
	            String inflater = Context.LAYOUT_INFLATER_SERVICE;
	            LayoutInflater vi;
	            vi = (LayoutInflater)getContext().getSystemService(inflater);
	            vi.inflate(resource, alertView, true);
	        }
	        else
	        {
	            alertView = (LinearLayout) convertView;
	        }

	        TextView alertText =(TextView)alertView.findViewById(R.id.txtAlertText);
	        TextView alertDate =(TextView)alertView.findViewById(R.id.txtAlertDate);
	        ImageButton imgBtn = (ImageButton)alertView.findViewById(R.id.apUnlockButton);
	         
	        alertText.setText(al.getName());
	        alertDate.setText(al.getStatus());
	        if(al.getStatus().equals("not_listed")){
	        	imgBtn.setEnabled(false);
	        }else{
	        	imgBtn.setEnabled(true);
	        }
	        
	         
	        return alertView;
	    }
		
}
