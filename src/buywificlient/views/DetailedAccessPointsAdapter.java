package buywificlient.views;

import java.util.List;

import com.example.buywificlient.R;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import buywificlient.MainActivity;
import buywificlient.wifi.WifiAP;
import c.mpayments.android.PurchaseManager;
import c.mpayments.android.PurchaseListener;
import c.mpayments.android.PurchaseRequest;
import c.mpayments.android.PurchaseResponse;

public class DetailedAccessPointsAdapter extends AccessPointsAdapter {

	public DetailedAccessPointsAdapter(Context context, int resource, List<WifiAP> items) {
		super(context, resource, items);
	}

	@Override
	public View getView(final int position, View convertView, ViewGroup parent) {
		
        LinearLayout alertView;
        final WifiAP al = getItem(position);
         
        if(convertView==null)
        {
            alertView = new LinearLayout(getContext());
            String inflater = Context.LAYOUT_INFLATER_SERVICE;
            LayoutInflater vi;
            vi = (LayoutInflater)getContext().getSystemService(inflater);
            vi.inflate(resource, alertView, true);
        }
        else
        {
            alertView = (LinearLayout) convertView;
        }

        TextView alertText =(TextView)alertView.findViewById(R.id.apNameText);
        TextView alertDate =(TextView)alertView.findViewById(R.id.apBssidText);
        ImageView imgView = (ImageView) alertView.findViewById(R.id.apStatusImage);
        ImageButton imgBtn = (ImageButton) alertView.findViewById(R.id.apUnlockButton);
             
        OnClickListener payClick = new OnClickListener() {
			
			@Override
			public void onClick(View v) {

				buywificlient.tools.PurchaseManager mngr = new buywificlient.tools.PurchaseManager();
				mngr.purchase(al);
				
			}
		};
        imgBtn.setOnClickListener(payClick);
        
        alertText.setText(al.getName());
        alertDate.setText(al.getBSSID());
        
        if(al.getStatus().equals("not_listed")){
        	imgView.setVisibility(View.INVISIBLE);
        	imgBtn.setEnabled(false);
        	imgBtn.setVisibility(View.INVISIBLE);
        }
        if(al.getStatus().equals("not_purchased")){
        	imgView.setVisibility(View.INVISIBLE);
        	imgBtn.setEnabled(true);
        	imgBtn.setVisibility(View.VISIBLE);
        }
        if(al.getStatus().equals("1h") || al.getStatus().equals("2h") || al.getStatus().equals("24h")){
        	imgView.setVisibility(View.VISIBLE);
        	imgBtn.setEnabled(false);
        	imgBtn.setVisibility(View.VISIBLE);
        }
         
        return alertView;
	}

	
}
