package buywificlient.tools;

import c.mpayments.android.PurchaseListener;
import c.mpayments.android.PurchaseRequest;
import c.mpayments.android.PurchaseResponse;
import android.util.Log;
import android.widget.Toast;
import buywificlient.MainActivity;
import buywificlient.wifi.WifiAP;
import buywificlient.wifi.WifiHelper;

public class PurchaseManager {
	
	public void purchase(WifiAP accessPoint){
		Log.d("PurchaseManager", "Trying to buy internet packet for AccessPoint: "+ accessPoint.getName() + ", MAC: "+accessPoint.getBSSID());
		
		PurchaseListener listener = new PurchaseListener() {
			
			@Override
			public void onPurchaseSuccess(PurchaseResponse arg0) {
				Log.d("PurchaseListener", "Purchase successfull!");
				Toast.makeText(MainActivity.getContext(), "Purchase successfull!", Toast.LENGTH_SHORT).show();
			}
			
			@Override
			public void onPurchasePending(PurchaseResponse arg0) {
				Log.d("PurchaseListener", "Purchase pending!");
				Toast.makeText(MainActivity.getContext(), "Purchase pending!", Toast.LENGTH_SHORT).show();
			}
			
			@Override
			public void onPurchaseFailed(PurchaseResponse arg0) {
				Log.d("PurchaseListener", "Purchase failed!");
				Toast.makeText(MainActivity.getContext(), "Purchase failed!", Toast.LENGTH_SHORT).show();
			}
			
			@Override
			public void onPurchaseCanceled(PurchaseResponse arg0) {
				Log.d("PurchaseListener", "Purchase canceled!");
				Toast.makeText(MainActivity.getContext(), "Purchase canceled!", Toast.LENGTH_SHORT).show();
			}
		};
		
		c.mpayments.android.PurchaseManager.attachPurchaseListener(listener);

		c.mpayments.android.PurchaseRequest pr = new c.mpayments.android.PurchaseRequest("b1127f77b98dad71b5d4732a0c338bb8"); 
		String userMac =  WifiHelper.getDeviceMACAddress();
		pr.setClientId(userMac+"I"+accessPoint.getBSSID()); // optional 
		 pr.setInfo("Internet package for access point: "+accessPoint.getBSSID()+", for user:"+userMac); // optional 
		 pr.setLanguageCode("HR"); // optional 
		 pr.setOfflineModeEnabled(true); // optional 
		 c.mpayments.android.PurchaseManager.startPurchase(pr, MainActivity.getContext());

	}

}
