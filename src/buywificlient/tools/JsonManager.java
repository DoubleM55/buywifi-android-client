package buywificlient.tools;

import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import buywificlient.wifi.WifiAP;

public class JsonManager {

	@SuppressWarnings("unchecked")
	public String encodeAPList(ArrayList<WifiAP> aplist, String clientMac) {

		JSONObject obj = new JSONObject();
		obj.put("userMac", clientMac);

		JSONArray array = new JSONArray();
		for (WifiAP ap : aplist) {
			JSONObject apObj = new JSONObject();
			apObj.put("mac", ap.getBSSID());
			apObj.put("ssid", ap.getSSID());
			array.add(apObj);
		}

		obj.put("discoveredWifis", array);

		return obj.toJSONString();

	}

	public ArrayList<WifiAP> decodeAPList(String jsonObject) {
		ArrayList<WifiAP> newList = new ArrayList<WifiAP>();

		System.out.println("decoding json: " + jsonObject);

		JSONParser parser = new JSONParser();

		try {
			JSONObject jObject = (JSONObject) parser.parse(jsonObject);

			JSONArray jsonArray = (JSONArray) jObject.get("validatedWifis");
			System.out.println("JsonManager Found " + jsonArray);

			Iterator i = jsonArray.iterator();
			while (i.hasNext()) {
				JSONObject elem = (JSONObject) i.next();
				String mac = (String) elem.get("mac");
				System.out.println("JsonManager Found " + mac + (String) elem.get("ssid") + (String) elem.get("state"));

				WifiAP ap = new WifiAP((String) elem.get("ssid"), (String) elem.get("mac"), (String) elem.get("ssid"));
				ap.setStatus((String) elem.get("state"));
				newList.add(ap);
			}

		} catch (ParseException pe) {
			System.out.println(pe);
		}

		return newList;
	}
}
