package buywificlient.network;

import java.util.ArrayList;
import java.util.List;

import android.os.AsyncTask;
import android.util.Log;
import buywificlient.MainActivity;
import buywificlient.wifi.APList;
import buywificlient.wifi.WifiAP;

public class WifiChecher extends AsyncTask<ArrayList<WifiAP>, WifiAP, APList>{
	private List<WifiAP> checkedAPs;
	
	void WifiChecker(){
		this.checkedAPs = new ArrayList<WifiAP>();
	}
	
	
	
	public WifiAP getAPAtIndex(int index){
		if(index < 0 || index >= this.checkedAPs.size()){
			return null;
		}else{
			return this.checkedAPs.get(index);
		}
		
	}
	
	public boolean contains(WifiAP ap){
		if(this.checkedAPs == null){
			this.checkedAPs=new ArrayList<WifiAP>();
			return false;
		}
		boolean c=false;
		for(WifiAP a : this.checkedAPs){
			if(a.equals(ap)){
				c=true;
			}
		}
		return c;
		
	}
	
	private void checkAPStatusPOST(WifiAP ap){
		if(ap == null){
			Log.e("WifiChecker", "checkAPStatusPOST GOT NULL POINTER!");
		}
		Log.d("WifiChecker","* (SENDING POST REQUEST) - Checking Access Point: "+ap.getName()+", BSSID: "+ap.getBSSID());
	}
	
	public void addNewAP(WifiAP ap){
		if(ap == null){
			Log.d("WifiChecker", "Access Point GOT NULL POINTER TO AP! "+ap.getName());
		}else{
			
			if(!contains(ap)){
				if(this.checkedAPs == null){
					this.checkedAPs = new ArrayList<WifiAP>();
				}
				this.checkedAPs.add(ap);
				System.out.println("WifiChecker got ap: "+ap.getName());
				
			}else{
				Log.d("WifiChecker", "Access Point Already Exists! "+ap.getName());
			}
			cancel(true);			
			
		}

	}
	
	public void addAllAp(List<WifiAP> accessibleAPs){
		try{
			
			if(accessibleAPs != null){
				this.checkedAPs.addAll(accessibleAPs);
			}else{
				Log.d("WifiChecker","No new updates (Empty list)");
			}
			
		}catch(NullPointerException e){
			Log.d("WifiChecker","No new updates (Empty list) Catched exception for null pointer");
		}

		
	}
	

	@Override
	protected void onPostExecute(APList result) {
		Log.d("WifiChecker","onPostExecute");

	}

	@Override
	protected void onProgressUpdate(WifiAP... values) {
		Log.d("WifiChecker","onProgressUpdate");

	}

	@Override
	protected APList doInBackground(ArrayList<WifiAP>... params) {
		Log.d("WifiChecker","doinBackground");
		for(WifiAP a : params[0]){
			this.addNewAP(a);
		}
		
		try{
			if(this.checkedAPs != null){
				if(this.checkedAPs.size()>0){
					for(WifiAP ap : this.checkedAPs){
						if(ap != null){
							checkAPStatusPOST(ap);
						}
						
					}						
				}
	
			}
	
		}catch(Exception e){
			e.printStackTrace();
		}

		this.cancel(true);
		return null;
	}



	public List<WifiAP> getCheckedAPs() {
		return checkedAPs;
	}





	
}
