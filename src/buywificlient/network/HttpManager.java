package buywificlient.network;

import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;
import org.apache.http.util.EntityUtils;

import buywificlient.MainActivity;
import android.util.Log;

public class HttpManager {
	private ArrayList<HttpResponseListener> listeners;
	
	public HttpManager(){
		listeners = new ArrayList<HttpResponseListener>();
	}
	
	public void registerResposneListener(HttpResponseListener l){
		Log.d("HttpMnager", "Addin new listener!");
		this.listeners.add(l);
	}
	
	private void publish(String response){
		Log.d("HttpMnager", "publish!");
		final String param = response;
		MainActivity.getInstance().runOnUiThread(new Runnable(){

			@Override
			public void run() {
				// TODO Auto-generated method stub
				MainActivity.getInstance().onResponseRecieved(param);
			}
			
			
		});
		
	}
	
	public void postData(String data) {
		Log.d("HttpManager", "Sending POST Request");
	    // Create a new HttpClient and Post Header
	    HttpClient httpclient = new DefaultHttpClient();
	    HttpPost httppost = null;
		try {
			httppost = new HttpPost(new URI("http://buywifi-5.milutindzunic.cloudbees.net/checkWifis"));
		} catch (URISyntaxException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

	    try {
	        // Add your data
	        List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>(2);
	        //nameValuePairs.add(new BasicNameValuePair("prvi", "Ovo je moj prvi parametar!"));
	        nameValuePairs.add(new BasicNameValuePair("json", data));
	        httppost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

	        // Execute HTTP Post Request
	        HttpResponse response = httpclient.execute(httppost);
	        HttpEntity entity = response.getEntity();
	        String responseString = EntityUtils.toString(entity, "UTF-8");
	        Log.d("Got POST RESPONSE",responseString);
	        //System.out.println(responseString);
	        
	        publish(responseString);

	    } catch (ClientProtocolException e) {
	        Log.e("HttpManager", "Got Eception!");
	        e.printStackTrace();
	    } catch (IOException e) {
	    	Log.e("HttpManager", "Got Eception!");
	    	e.printStackTrace();
	    }
	} 
	
	public void getData(){
		 HttpResponse response;
		try {        
	        HttpClient client = new DefaultHttpClient();
	        HttpGet request = new HttpGet();
	        request.setURI(new URI("http://192.168.1.44:8080/Buy_Wifi/TestServlet"));
	        
	        
	        response = client.execute(request);
	    } catch (URISyntaxException e) {
	        e.printStackTrace();
	    } catch (ClientProtocolException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    } catch (IOException e) {
	        // TODO Auto-generated catch block
	        e.printStackTrace();
	    }   

	}
}
