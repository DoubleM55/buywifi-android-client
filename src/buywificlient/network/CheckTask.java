package buywificlient.network;

import java.util.ArrayList;

import buywificlient.tools.JsonManager;
import buywificlient.wifi.WifiAP;
import buywificlient.wifi.WifiHelper;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;

public class CheckTask extends AsyncTask<ArrayList<WifiAP>, WifiAP, ArrayList<WifiAP>> {
	
	private boolean isAPPresent(ArrayList<WifiAP> list, WifiAP ap){
		boolean exists = false;
		
		for(WifiAP a : list){
			if(a.getBSSID().equals(ap.getBSSID())){
				exists = true;
			}
		}
		
		return exists;
	}

	private ArrayList<WifiAP> removeDoubles(ArrayList<WifiAP> list){
		ArrayList<WifiAP> newlist = new ArrayList<WifiAP>();
		for(WifiAP ap : list){
			if(!isAPPresent(newlist, ap)){
				newlist.add(ap);
			}
		}
		return newlist;
		
	}

	@Override
	protected ArrayList<WifiAP> doInBackground(ArrayList<WifiAP>... params) {
		Log.d("CheckTask", "Got list: ");
		ArrayList<WifiAP> cleanList = removeDoubles(params[0]);
		for(WifiAP ap : cleanList){
			Log.d("CheckTask", "AP: "+ap.getName());
		}
		
		JsonManager json = new JsonManager();
		String requestParam = json.encodeAPList(cleanList, WifiHelper.getDeviceMACAddress());
		HttpManager mngr = new HttpManager();
		mngr.postData(requestParam);
		System.out.println(requestParam);
		
		return cleanList;
	}

}
