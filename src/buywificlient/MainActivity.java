package buywificlient;

import java.util.ArrayList;
import java.util.List;

import com.example.buywificlient.R;
import com.infobip.push.PushNotificationManager;

import buywificlient.network.CheckTask;
import buywificlient.network.HttpManager;
import buywificlient.network.HttpResponseListener;
import buywificlient.network.WifiChecher;
import buywificlient.tools.JsonManager;
import buywificlient.views.AccessPointsAdapter;
import buywificlient.views.DetailedAccessPointsAdapter;
import buywificlient.wifi.APList;
import buywificlient.wifi.APScanner;
import buywificlient.wifi.FakeWifiScanner;
import buywificlient.wifi.WifiAP;
import buywificlient.wifi.WifiScanner;
import android.net.wifi.WifiManager;
import android.os.Bundle;
import android.os.Handler;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.IntentFilter;
import android.util.Log;
import android.view.Menu;
import android.view.View;
import android.widget.ListView;
import android.widget.TextView;

public class MainActivity extends Activity implements HttpResponseListener{
	private static Context context;
	private static MainActivity instance;
	ProgressDialog ringProgressDialog = null;
	List<WifiAP> aps;
	ArrayList<WifiAP> accessibleAPs = new ArrayList<WifiAP>();
	final Handler h = new Handler();
	APScanner apScanner;
	private PushNotificationManager pushManager;
	
	private HttpManager httpManager;
	
	public MainActivity(){
	}
	
	public HttpManager getHttpManager() {
		return httpManager;
	}

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		
		this.aps = new ArrayList<WifiAP>();
		this.accessibleAPs = new ArrayList<WifiAP>();
		
		MainActivity.instance = this;
		MainActivity.context = getApplicationContext();
		
		this.apScanner = new APScanner();
		httpManager = new HttpManager();
		httpManager.registerResposneListener(this);
		
		// Push notifications	
		/*
	    pushManager = new PushNotificationManager(this);
	    pushManager.checkManifest();
	    pushManager.setDebugModeEnabled(true); //Enable debug mode to view log
	    pushManager.initialize("261657561058", "08ef3474ced2", "620a3cc62963");		// Ovo je sad pode�eno sa postavkama 
	    
	    pushManager.register();
	    */
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}
	
	public void discoverNetworks(View v){
		ringProgressDialog = ProgressDialog.show(MainActivity.this, "Please wait ...", "Discovering WiFi Access Points ...", true);
		ringProgressDialog.setCancelable(true);
		
		System.out.println("Scanning for networks...");
		this.aps = new ArrayList<WifiAP>();
		this.accessibleAPs = new ArrayList<WifiAP>();
		
		apScanner.beginScan();
		
		
	    h.postDelayed(new Runnable(){

		@Override
		public void run() {
			ringProgressDialog.dismiss();
			
			updateAPList(apScanner.getLista());
			checkStatus(apScanner.getLista());
		}
    	   
       }, 5000);
		
	}
	
	private boolean isApPresent(WifiAP accp){
		boolean present = false;
		
		for(WifiAP apoint : this.aps){
			if(apoint.getBSSID().equals(accp.getBSSID())){
				present = true;
			}
		}
		
		return present;
	}
	
	public void unlockAP(WifiAP ap){
		Log.d("MainActivity", "***** UNLOCKING AP: "+ap.getName());
	}
	
	public void addAPtoList(WifiAP ap){
		Log.d("MainActivity", "addAPtoList");
		if(!isApPresent(ap)){
			this.aps.add(ap);
			this.accessibleAPs.add(ap);
			
			this.updateAPList(apScanner.getLista()); 
		}else{
			this.updateAPList(apScanner.getLista());
		}

	}
	
	public void updateAPList(ArrayList<WifiAP> list){

		Log.d("MainActivity", "updateAPList");
		if(list == null){
			System.out.println("Update list  NULL POINTER !!!!!!!!!!");
		}

		ListView lstTest= (ListView)findViewById(R.id.lstText);
		if(list.size() == 0){
			Log.d("MainActivity", "Empty List!");
		}
		
		ArrayList<WifiAP> alrts = new ArrayList<WifiAP>();
		AccessPointsAdapter arrayAdapter = new DetailedAccessPointsAdapter(this, R.layout.ap_list_item, alrts);
       lstTest.setAdapter(arrayAdapter);
        
       try
       {
    	   for(WifiAP l : list)
           {
        	   alrts.add(l);
           }

           arrayAdapter.notifyDataSetChanged();
       }
       catch(Exception e)
       {
           Log.d("Error: ", e.getMessage());
       }

	}
	
	@SuppressWarnings("unchecked")
	public void checkStatus(ArrayList<WifiAP> foundAP){
		Log.d("MainActivity", "checkStatus");
		
		CheckTask ctask = new CheckTask();
		ctask.execute(foundAP);
	}

	public void clearAPList(){
		Log.d("MainActivity", "clearAPList");
	}
	
	public static MainActivity getInstance() {
		return instance;
	}

	public static Context getContext() {
		return context;
	}

	public List<WifiAP> getAccessibleAPs() {
		return accessibleAPs;
	}

	public void updateAPList(ArrayList<WifiAP> list, boolean checkStatuses) {
		
	}

	@Override
	public void onResponseRecieved(String jsonObject) {
		Log.d("MainActivity", "HttpResponse recieved: "+jsonObject);
		JsonManager m = new JsonManager();
		ArrayList<WifiAP> lista = m.decodeAPList(jsonObject);
		for(WifiAP ap: lista){
			System.out.println(ap.getName());
		}
		updateAPList(lista);
	}
	
	
	
}
