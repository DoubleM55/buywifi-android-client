package buywificlient.wifi;

import java.util.ArrayList;
import java.util.List;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.Handler;
import android.util.Log;
import buywificlient.MainActivity;

public class APScanner {
	private Handler handler;
	final ArrayList<WifiAP> lista;

	public APScanner() {
		lista = new ArrayList<WifiAP>();

	}

	public ArrayList<WifiAP> getLista() {
		return lista;
	}

	public void beginScan() {

		Log.d("WifiScanner", "doInBackground");
		this.lista.clear();

		final WifiManager wifiManager = (WifiManager) MainActivity.getContext().getSystemService(Context.WIFI_SERVICE);
		MainActivity.getContext().registerReceiver(new BroadcastReceiver() {

			@Override
			public void onReceive(Context arg0, Intent arg1) {
				lista.clear();
				List<ScanResult> results = wifiManager.getScanResults();
				for (ScanResult ap : results) {
					Log.d("WifiScanner", "SSID=" + ap.SSID + " MAC=" + ap.BSSID);

					WifiAP accp = new WifiAP(ap.SSID, ap.BSSID, ap.SSID);
					lista.add(accp);
					Log.d("WifiScanner", "Access Point : " + ap.SSID);
				}

			}
		}, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION));
		wifiManager.startScan();
	}

}
