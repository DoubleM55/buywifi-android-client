package buywificlient.wifi;

public class WifiAP {
	private String SSID;
	private String BSSID;
	private String Name;
	private String status;
	
	public WifiAP() {
		
	}
	
	public WifiAP(String sSID, String bSSID, String name) {
		SSID = sSID;
		BSSID = bSSID;
		Name = name;
		status = "not_listed";
	}
	
	public String getSSID() {
		return SSID;
	}
	public void setSSID(String sSID) {
		SSID = sSID;
	}
	public String getBSSID() {
		return BSSID;
	}
	public void setBSSID(String bSSID) {
		BSSID = bSSID;
	}
	public String getName() {
		return Name;
	}
	public void setName(String name) {
		Name = name;
	}
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	@Override
	public boolean equals(Object o) {
		WifiAP ap = (WifiAP) o;
		boolean eq=true;
		if(ap.getBSSID().equals(this.BSSID)){
			eq=false;
		}
		if(ap.getSSID().equals(this.SSID)){
			eq=false;
		}
		if(ap.getName().equals(this.Name)){
			eq=false;
		}
		if(ap.getStatus().equals(this.status)){
			eq=false;
		}
		return eq;
	}
	
	
}
