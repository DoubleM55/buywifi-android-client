package buywificlient.wifi;

import buywificlient.MainActivity;
import android.content.Context;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.util.Log;

public class WifiHelper {
	
	public static String getDeviceMACAddress(){
		WifiManager wifiMan = (WifiManager) MainActivity.getInstance().getSystemService(
        Context.WIFI_SERVICE);
		WifiInfo wifiInf = wifiMan.getConnectionInfo();
		String macAddr = wifiInf.getMacAddress();
		Log.d("WifiHelper", "Got device MAC address: "+macAddr);
		return macAddr;
	}

}
