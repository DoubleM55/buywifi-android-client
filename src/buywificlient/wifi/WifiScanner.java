package buywificlient.wifi;

import java.util.List;

import buywificlient.MainActivity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.wifi.ScanResult;
import android.net.wifi.WifiManager;
import android.os.AsyncTask;
import android.util.Log;

public class WifiScanner extends AsyncTask<Void, WifiAP, APList>{
	protected APList apList;

	public WifiScanner() {
		this.apList = new APList();
	}

	@Override
	protected void onPreExecute() {
		Log.d("WifiScanner","WifiScanner Started!");
		this.apList = new APList();
		MainActivity.getInstance().clearAPList();
	}

	private boolean isAPPresent(WifiAP ap){
		boolean present = false;
		
		for(WifiAP apoint : (apList.getApList())){
			if(ap.getBSSID().equals(apoint.getBSSID())){
				present = true;
			}
		}
		
		return present;
	}

	@Override
	protected APList doInBackground(Void... params) {
		Log.d("WifiScanner","doInBackground");
		this.apList.clear();
		
		final WifiManager wifiManager = (WifiManager) MainActivity.getContext().getSystemService(Context.WIFI_SERVICE);
		MainActivity.getContext().registerReceiver(new BroadcastReceiver() {

				@Override
				public void onReceive(Context arg0, Intent arg1) {
					// TODO Auto-generated method stub
			           List<ScanResult> results = wifiManager.getScanResults();
			           for (ScanResult ap : results) {
			               Log.d("WifiScanner", "SSID=" + ap.SSID + " MAC=" + ap.BSSID); 
			               
			               WifiAP accp = new WifiAP(ap.SSID, ap.BSSID, ap.SSID);
			               if(isAPPresent(accp)){
			            	   Log.d("WifiScanner", "Access Point already exists!");
			               }else{
			            	   apList.addAP(accp);
			            	   publishProgress(accp);
			               }

			           }
			           
					
				}
		}, new IntentFilter(WifiManager.SCAN_RESULTS_AVAILABLE_ACTION)); 
		wifiManager.startScan();

		
		return this.apList;
	}

	

	@Override
	protected void onProgressUpdate(WifiAP... values) {
		Log.d("WifiBroadcastReciever","onProgressUpdate");
		
		
		for(WifiAP ap : values){
			Log.i("WifiScanner", "Found WiFi Access Point: " + ap.getBSSID());
			MainActivity.getInstance().addAPtoList(ap);
		}
		
		
		
	}

	@Override
	protected void onPostExecute(final APList result) {
		Log.d("WifiScanner","onPostExecute");
		
	}
	

}
