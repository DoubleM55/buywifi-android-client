package buywificlient.wifi;

public class FakeWifiScanner extends WifiScanner {

	@Override
	protected void onPreExecute() {
		super.onPreExecute();
	}

	@Override
	protected APList doInBackground(Void... params) {
		this.apList.addAP(new WifiAP("HomeWLAN","AB:93:01:BC:E8","Home WLAN Network"));
		this.apList.addAP(new WifiAP("Fabro","A3:23:33:21:E3","Fabro Network"));
		this.apList.addAP(new WifiAP("tkYrzs","34:33:E2:CC:D2","<unknown>"));
		this.apList.addAP(new WifiAP("SpeedTouch","12:A4:D8:ED:75","Speed Touch Network"));
		
		return super.doInBackground(params);
	}

	@Override
	protected void onProgressUpdate(WifiAP... values) {
		super.onProgressUpdate(values);
	}

	@Override
	protected void onPostExecute(APList result) {
		super.onPostExecute(result);
	}

}
