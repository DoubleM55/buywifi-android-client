package buywificlient.wifi;

import java.util.ArrayList;
import java.util.List;

public class APList {
	private List<WifiAP> apList;
	
	APList(){
		this.apList = new ArrayList<WifiAP>();
	}
	
	public APList(List<WifiAP> list){
		this.apList = list;
	}
	
	public void addAP(WifiAP ap){
		this.apList.add(ap);
	}

	public List<WifiAP> getApList() {
		return apList;
	}
	
	public WifiAP getAPByIndex(int index){
		return apList.get(index);
	}
	
	public void clear(){
		this.apList.clear();
	}

	
}
